/*
 * Copyright 2015 Javier Casanova Hernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jch.sqliblite.statements.ddl;

/**
 * Representa una columna de una tabla de la base de datos
 *
 * Created by javier on 18/06/15.
 */
public class Colum {

    /**
     * Crea una cadena con el nombre de la columan y el tipo de dato
     *
     * @param columnName
     * @param dataType
     * @return
     */
    public static String value(String columnName, String dataType) {
        return String.format("%s %s", columnName, dataType);
    }

    /**
     * Crea una cadena con el nombre de la columna, el tipo de dato y las
     * restricciones que se quieran aplicar al campo
     *
     * @param columnName
     * @param dataType
     * @param constraint
     * @return
     */
    public static String value(String columnName, String dataType, String constraint) {
        return String.format("%s %s %s", columnName, dataType, constraint);
    }
}
