/*
 * Copyright 2015 Javier Casanova Hernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jch.sqliblite.statements.ddl;

import com.jch.sqliblite.IStatement;
import com.jch.sqliblite.util.Util;

/**
 * Representa la query que crea una nueva tabla en la base de datos
 *
 * Created by javier on 18/06/15.
 */
public class CreateTable implements IStatement {

    private final StringBuilder query;

    /**
     * Constructor
     */
    public CreateTable() {
        this.query = new StringBuilder();
    }

    /**
     * Inicializa el objeto con el nombre de la tabla
     *
     * @param name nombre de la tabla
     * @return
     */
    public static CreateTable name(String name) {
        CreateTable createTable = new CreateTable();
        createTable.query.append("CREATE TABLE ");
        createTable.query.append(name);
        return createTable;
    }

    /**
     * Concatena las columnas que se quieran crear en la tabla
     *
     * @param colums
     * @return
     */
    public CreateTable colums(String... colums) {
        query.append("(");
        Util.appendUpdateArgument(query, colums);
        query.append(");");
        return this;
    }

    @Override
    public String builStatement() {
        query.trimToSize();
        return query.toString();
    }
}
