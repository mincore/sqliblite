/*
 * Copyright 2015 Javier Casanova Hernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jch.sqliblite.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jch.sqliblite.data.exception.DatabaseCreationException;

/**
 * Clase abstracta que representa una base de datos. Para crear un objeto que represente una
 * base de datos fisica es necesario crear una subclase e implementar los metodos abstractos.
 * <p/>
 * El uso de esta clase abstrae al usuario de las operaciones comunes al crear y gestionar una
 * base de datos.
 *
 * @author javier
 */
public abstract class AbstractDatabase {

    private final Context context;
    private SQLiteOpenHelper helper;
    private SQLiteDatabase database;

    /**
     * Constructor
     *
     * @param ctx Contexto de la aplicacion
     */
    public AbstractDatabase(Context ctx) {
        context = ctx.getApplicationContext();
    }

    /**
     * Nombre de la base de datos. Usado para construir el {@link com.jch.sqliblite.data.AbstractDatabase.DatabaseHelper}
     *
     * @return Cadena que representa el nombre de la base de datos
     */
    public abstract String getName();

    /**
     * Version de la base de datos. Usado para construir el {@link com.jch.sqliblite.data.AbstractDatabase.DatabaseHelper}
     *
     * @return Entero que representa la version de la base de datos
     */
    public abstract int getVersion();

    /**
     * Operaciones que se tienen que ejecutar al crear una nueva base de datos. Es usado para
     * construir el {@link com.jch.sqliblite.data.AbstractDatabase.DatabaseHelper}.
     * <p/>
     * Para entender el funcionamiento de este metodo y comprender las operaciones que realiza
     * consultar {@link SQLiteOpenHelper#onCreate(SQLiteDatabase)}.
     *
     * @param database
     */
    protected abstract void executeCreate(SQLiteDatabase database);

    /**
     * Operaciones que se tienen que ejecutar al actualizar a una nueva version de la base de datos.
     * Es usado para construir el {@link com.jch.sqliblite.data.AbstractDatabase.DatabaseHelper}.
     * <p/>
     * Para entender el funcionamiento de este metodo y comprender las operaciones que realiza
     * consultar {@link SQLiteOpenHelper#onUpgrade(SQLiteDatabase, int, int)}.
     * <p/>
     * Al actualizar se borran las tablas y se pierde toda la informacion
     *
     * @param database
     * @param oldVer Entero que representa la version antigua de la base de datos
     * @param newVer Entero que representa la nueva version de la base de datos
     */
    protected abstract void executeUpgrade(SQLiteDatabase database, int oldVer, int newVer);

    /**
     * Inicializa el objeto {@link SQLiteDatabase} de esta clase.
     * <p/>
     * Abre una nueva conexion a la base de datos para realizar una determinada operacion. No puede
     * ser llamado nunca simultaneamente por dos metodos.
     * <p/>
     * Es el propio framework el que se encarga de realizar la llamada a este metodo y no puede ser
     * sobreescrito por ninguna subclase
     *
     * @return
     */
    protected synchronized final SQLiteDatabase openDatabase() {
        if (database == null) {
            open();
        }
        return database;
    }

    /**
     * Destruye el objeto {@link SQLiteDatabase} de esta clase.
     * <p/>
     * Cierra la conexion a la base de datos. No puede ser llamado nunca simultaneamente por dos metodos.
     * <p/>
     * Es el propio framework el que se encarga de realizar la llamada a este metodo y no puede ser
     * sobreescrito por ninguna subclase
     */
    protected synchronized final void closeDatabase() {
        if (database.isOpen()) {
            database.close();
            helper.close();
            database = null;
            helper = null;
        }
    }

    /**
     * Abre una conexion a la base de datos en modo escritura.
     */
    private void open() {
        initializeHelper();
        try {
            database = helper.getWritableDatabase();
        } catch (DatabaseCreationException e) {
            logError(e.getMessage(), e);
        }
    }

    /**
     * Inicializa el {@link com.jch.sqliblite.data.AbstractDatabase.DatabaseHelper}
     */
    private void initializeHelper() {
        if (helper == null) {
            helper = new DatabaseHelper(context, getName(), null, getVersion());
        }
    }

    /**
     * Imprime un mensage de error en la consola
     *
     * @param msg mensage
     * @param error error
     */
    private void logError(String msg, Throwable error) {
        Log.e(getClass().getSimpleName(), msg, error);
    }

    /**
     * Subclase de {@link SQLiteOpenHelper}
     */
    private final class DatabaseHelper extends SQLiteOpenHelper {

        /**
         * Constructor
         *
         * @param context
         * @param name
         * @param factory
         * @param version
         */
        public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqld) {
            executeCreate(sqld);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqld, int oldVersion, int newVersion) {
            Log.w(AbstractDatabase.class.getSimpleName(), String.format(
                "Upgrading database from %s to %s, which will destroy all data", oldVersion, newVersion));
            executeUpgrade(database, oldVersion, newVersion);
            onCreate(sqld);
        }
    }
}
