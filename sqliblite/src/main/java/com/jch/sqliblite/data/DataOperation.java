/*
 * Copyright 2015 Javier Casanova Hernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jch.sqliblite.data;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que reresenta las posibles operaciones que se pueden realizar contra
 * la base de datos, como pueden ser insertar nuevos valores, actualizarlos,
 * eliminarlos o seleccionarlos
 *
 * @author javier
 */
public final class DataOperation {

    private final AbstractDatabase database;

    /**
     * Constructor
     *
     * @param db base de datos sobre la que se van a realizar las operaciones
     */
    public DataOperation(AbstractDatabase db) {
        database = db;
    }

    /**
     * Ejecuta una query creada mediante {@link com.jch.sqliblite.statements.Insert}
     * sobre la base de datos.
     *
     * @param statement Query que se va a ejecutar
     * @return numero de fila insertada
     */
    public long insert(String statement) {
        try {
            return database.openDatabase().compileStatement(statement).executeInsert();
        } finally {
            database.closeDatabase();
        }
    }

    /**
     * Ejecuta una query creada mediante {@link com.jch.sqliblite.statements.Update}
     * sobre la base de datos.
     *
     * @param statement query
     * @return numero de registros actualizados en la base de datos
     */
    public int update(String statement) {
        try {
            return database.openDatabase().compileStatement(statement).executeUpdateDelete();
        } finally {
            database.closeDatabase();
        }
    }

    /**
     * Ejecuta una query creada mediante {@link com.jch.sqliblite.statements.Delete}
     * sobre la base de datos.
     *
     * @param statement query
     * @return numero de registros eliminados en la base de datos
     */
    public int delete(String statement) {
        try {
            return database.openDatabase().compileStatement(statement).executeUpdateDelete();
        } finally {
            database.closeDatabase();
        }
    }

    /**
     * Ejecuta una query creada mediante {@link com.jch.sqliblite.statements.Select}
     * sobre la base de datos.
     * <p/>
     * Utiliza {@link EntityMapper} para obtener los valores del cursor y pasarlos
     * a un {@link List}.
     *
     * @param statement query
     * @param mapper Mapeador de las entidades devueltas en un {@link Cursor}
     * @param <T> tipo de entidad
     * @return conjunto de valores devueltos por la consulta
     */
    public <T> List<T> get(String statement, EntityMapper<T> mapper) {
        Cursor cursor = null;
        List<T> entities = new ArrayList<>();
        try {
            cursor = database.openDatabase().rawQuery(statement, null);
            if(cursor.moveToFirst()) {
                do {
                    entities.add(mapper.mapEntity(cursor));
                } while (cursor.moveToNext());
            }
            return entities;
        } finally {
            if(cursor != null) cursor.close();
            database.closeDatabase();
        }
    }
}
