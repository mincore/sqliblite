/*
 * Copyright 2015 Javier Casanova Hernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jch.sqliblite.functions;

import com.jch.sqliblite.util.Util;

/**
 * Operaciones comunes a las clases {@link com.jch.sqliblite.statements.Update},
 * {@link com.jch.sqliblite.statements.Select}, y {@link com.jch.sqliblite.statements.Delete}
 * <p/>
 * Se encarga de concatenar en un {@link StringBuilder} las operaciones que se necesiten
 * para construir una consulta.
 *
 * @author javier
 */
public abstract class AbstractOperations {

    private final StringBuilder query;

    /**
     * Constructor
     *
     * @param q objeto usado para concatenar las operaciones
     */
    protected AbstractOperations(StringBuilder q) {
        query = q;
    }

    /**
     * Concatena la operacion AND a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param condition
     */
    protected final void andOperation(Object... condition) {
        query.append(" AND");
        Util.appendCondition(query, condition);
    }

    /**
     * Concatena la operacion BETWEEN a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param val1
     * @param val2
     */
    protected final void betweenOperation(Object val1, Object val2) {
        query.append(" BETWEEN ").append(val1).append(" AND ").append(val2);
    }

    /**
     * Concatena la operacion EXIST a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param subquery
     */
    protected final void existsOperation(Object subquery) {
        query.append(" EXISTS (").append(subquery).append(")");
    }

    /**
     * Concatena la operacion IN a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param values
     */
    protected final void inOperation(Object... values) {
        query.append(" IN (");
        Util.appendUpdateArgument(query, values);
        query.append(")");
    }

    /**
     * Concatena la operacion LIKE a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param value
     */
    protected final void likeOperation(Object value) {
        query.append(" LIKE ");
        Util.escapeString(query, value);
    }

    /**
     * Concatena la operacion GLOB a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param value
     */
    protected final void globOperation(Object value) {
        query.append(" GLOB ");
        Util.escapeString(query, value);
    }

    /**
     * Concatena la operacion NOT a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     */
    protected final void notOperation() {
        query.append(" NOT ");
    }

    /**
     * Concatena la operacion OR a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param condition
     */
    protected final void orOperation(Object condition) {
        query.append(" OR ").append(condition);
    }

    /**
     * Concatena la operacion IS NOT a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     *
     * @param value
     */
    protected final void isNotOperation(Object value) {
        query.append(" IS NOT ").append(value);
    }

    /**
     * Concatena la operacion IS NULL a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     */
    protected final void isNullOperation() {
        query.append(" IS NULL");
    }

    /**
     * Concatena la operacion IS NOT NULL a la consulta.
     * <p/>
     * Este metodo no puede ser sobreescrito por ninguna subclase
     */
    protected final void isNotNullOperation() {
        query.append(" IS NOT NULL");
    }
}
