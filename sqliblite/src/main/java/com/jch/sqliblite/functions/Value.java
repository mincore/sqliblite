/*
 * Copyright 2015 Javier Casanova Hernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jch.sqliblite.functions;

/**
 * Representan los valores pasados a una consulta del tipo {@link com.jch.sqliblite.statements.Update}
 * <p/>
 * No se debe llamar directamente al metodo toString ya que es el propio framework el encargado
 * de realizar esta operacion
 *
 * Created by javier on 19/06/15.
 */
public class Value {

    private final String column;
    private final Object value;

    /**
     * Constructor
     *
     * @param column nombre de la columna
     * @param value nuevo valor. Si es de tipo {@link String} lo escapa con comillas simples
     */
    public Value(String column, Object value) {
        this.column = column;
        this.value = value;
    }

    @Override
    public String toString() {
        if(value instanceof String) return String.format("%s = '%s'", column, value);
        return String.format("%s = %s", column, value);
    }
}
