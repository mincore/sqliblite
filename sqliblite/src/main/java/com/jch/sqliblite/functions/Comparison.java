/*
 * Copyright 2015 Javier Casanova Hernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jch.sqliblite.functions;

import com.jch.sqliblite.util.Util;

/**
 * Operaciones logicas
 *
 * @author javier
 */
public class Comparison {

    /**
     * Construye una nueva cadena con el operador '=' y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String equal(Object value) {
        if (value instanceof String) {
            StringBuilder sb = new StringBuilder();
            Util.escapeString(sb, value);
            return String.format("= %s", sb.toString());
        }
        return String.format("= %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '!=' y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String notEqual(Object value) {
        if (value instanceof String) {
            StringBuilder sb = new StringBuilder();
            Util.escapeString(sb, value);
            return String.format("!= %s", sb.toString());
        }
        return String.format("!= %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '<>' y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String nonEqual(Object value) {
        if (value instanceof String) {
            StringBuilder sb = new StringBuilder();
            Util.escapeString(sb, value);
            return String.format("<> %s", sb.toString());
        }
        return String.format("<> %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '>' (mayor que) y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String greater(Object value) {
        return String.format("> %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '<' (menor que) y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String less(Object value) {
        return String.format("< %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '>=' (mayor o igual que) y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String greaterOrEqual(Object value) {
        return String.format(">= %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '<=' (menor o igual que) y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String lessOrEqual(Object value) {
        return String.format("<= %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '!<' (no menor de) y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String notLess(Object value) {
        return String.format("!< %s", value);
    }

    /**
     * Construye una nueva cadena con el operador '!>' (no mayor que) y el valor pasado. Si el parametro es
     * de tipo {@link String} lo escapa con comillas simples.
     *
     * @param value
     * @return
     */
    public static String notGreater(Object value) {
        return String.format("!> %s", value);
    }
}
